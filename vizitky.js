//CORS shitcocks
var host;
if (location.hostname != 'localhost') {
    host = 'https://interaktivni.rozhlas.cz/data/volby-17-vizitky';
} else {
    host = '.';
}

function share(url) {
    window.open(url, 'Sdílení', 'width=550,height=450,scrollbars=no');
};

//social sharing
function makeSelect() {
    var id = location.href.split('?')[1];
    if (id != null) {
        var filtered = table[parseInt(id)]

        if (!filtered) {
            return;
        }

        var out = '<h1>' + filtered.kraj + (filtered.kraj == 'Praha' | filtered.kraj == 'Vysočina' ? '' : ' kraj') + '</h1><ul>';
       

        out += '<li><div class="right"><h2><span class="cislo">' 
        + filtered.KSTRANA 
        + '</span> <span>' 
        + filtered.JMENO 
        + ' ' 
        + filtered.PRIJMENI
        + '</span></h2><span class="strana">' 
        + filtered.partaj 
        + '</span> <span class="supplemental">' 
        + filtered.POVOLANI
        + (filtered.pozn != null ? '<div><span class="supplemental">' + filtered.pozn + '</span></div>' : '')
        + '</span>'
        + (filtered.mp3 != null ? '<div><audio src="https://samizdat.blob.core.windows.net/vizitky17/' + filtered.mp3 + '" preload="none" controls="yes"></audio></div>'  : '')
        + '</div><div class="left"><img width="120" height="180" alt="' 
        + filtered.JMENO
        + ' ' 
        + filtered.PRIJMENI
        + '" src="https://samizdat.blob.core.windows.net/vizitky17/'
        + filtered.foto 
        +'"></div></li>'
        out += '</ul>'
        $('.linked').html(out);

    } 
}


function makeTable(kraj) {
    var out = '<h1>' + kraj + (kraj == 'Praha' | kraj == 'Vysočina' ? '' : ' kraj') + '</h1><ul>';

    var filtered = table.filter(function(r) {
        return r.kraj == kraj;
    })

    for (var per in filtered) {
        out += '<li><div class="right"><h2><span class="cislo">' 
        + filtered[per].KSTRANA 
        + '</span> <span>' 
        + filtered[per].JMENO 
        + ' ' 
        + filtered[per].PRIJMENI
        + '</span></h2><span class="strana">' 
        + filtered[per].partaj 
        + '</span> <span class="supplemental">' 
        + filtered[per].POVOLANI
        + (filtered[per].pozn != null ? '<div><span class="supplemental">' + filtered[per].pozn + '</span></div>' : '')
        + '</span>'
        + '<span class="share">Sdílet na <a href="javascript:share(\'https://www.facebook.com/sharer/sharer.php?u=' + location.href.split('?')[0] + '?' + table.indexOf(filtered[per]) + '\');'
        + '">Facebook</a> | <a href="javascript:share(\'https://twitter.com/home?status=' + location.href.split('?')[0] + '?' + table.indexOf(filtered[per]) + '\');">Twitter</a></span>'
        + (filtered[per].mp3 != null ? '<div><audio src="https://samizdat.blob.core.windows.net/vizitky17/' + filtered[per].mp3 + '" preload="none" controls="yes"></audio></div>'  : '')
        + '</div><div class="left"><img width="120" height="180" alt="' 
        + filtered[per].JMENO
        + ' ' 
        + filtered[per].PRIJMENI
        + '" src="https://samizdat.blob.core.windows.net/vizitky17/'
        + filtered[per].foto 
        +'"></div></li>'
    }
    out += '</ul>'
    $('#bottom').html(out);
}

var table = [];
var kraje = [];

var mapka;

function selectMap(kraj) {
    if (kraj != undefined) {
        mapka.series[0].data.forEach(function(e) {
            if (e.NAME_1 == kraj) {
                mapka.series[0].data[mapka.series[0].data.indexOf(e)].select(true, false)
            }
        })
    }
};

$.getJSON(host + '/data/kraje.json', function (geojson) {
    mapka = Highcharts.mapChart('container', {
        title: {
            text: ''
        },
        subtitle: {
            align: 'center',
            useHTML: true,
            text: '<i>kliknutím do mapy vyberte kraj</i>'
        },
         legend: {
            enabled: false
        },
        credits: {
          enabled: false
        },
        plotOptions: {
            series: {
                color: '#036',
                nullColor: '#036',
                allowPointSelect: false,
                states: {
                    hover: {
                        color: '#ab0000',
                        brightness: 0.2,
                        enabled: true
                    },
                    select: {
                        color: '#ab0000'
                    }
                }
            }
        },
        series: [{
            type: 'map',
            tooltip: {
                headerFormat: '',
                pointFormat: 'Kraj {point.NAME_1}'
            },
            data: [{"NAME_1": "Ústecký", "val": 1}, {"NAME_1": "Jihočeský", "val": 1}, {"NAME_1": "Jihomoravský", "val": 1}, {"NAME_1": "Karlovarský", "val": 1}, {"NAME_1": "Královéhradecký", "val": 1}, {"NAME_1": "Vysočina", "val": 1}, {"NAME_1": "Liberecký", "val": 1}, {"NAME_1": "Moravskoslezský", "val": 1}, {"NAME_1": "Olomoucký", "val": 1}, {"NAME_1": "Pardubický", "val": 1}, {"NAME_1": "Plzeňský", "val": 1}, {"NAME_1": "Praha", "val": 1}, {"NAME_1": "Středočeský", "val": 1}, {"NAME_1": "Zlínský", "val": 1}],
            mapData: geojson,
            name: ' ',
            joinBy: ['NAME_1', 'NAME_1'],
            events: {
                click: function(e) {
                    if ('point' in e.target) {
                        makeTable(e.target.point.properties.NAME_1)
                        $('#select select').val(e.target.point.properties.NAME_1).change();
                    }  
                }
            }
        }]
    });

    $.getJSON(host + '/data/data.json', function (dta) {
        for (var i in dta) {
            table.push(dta[i])
            kraje.push(dta[i].kraj)
        }
        makeSelect();
        kraje = kraje.filter(function (v, i, a) {
            return a.indexOf(v) === i;
        });
        var selHTML = '<select><option value="#">Vyberte kraj...</option>'
        kraje.forEach(function(e) {
            selHTML += '<option value="' + e + '">' + e + '</option>'
        })
        selHTML += '</select>'
        $('#select').html(selHTML)

        $('#select').on('change', function(e) {
            if (e.target.value != '#') {
                selectMap(e.target.value)
                makeTable(e.target.value)     
            }       
        })
    })
});